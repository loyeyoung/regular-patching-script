# regular-patching-script

Code not uploaded yet. (But you probably already noticed that.)

Boring bash script to update system using yum or dnf, keep a log, decide whether to reboot, and reboot as appropriate. Not much more than a wrapper, but useful to me. 

## Dependencies
"source-variable" command, which is slightly more secure than the usual "source" or "." and which is or will be soon available at ~/loyeyoung/source-variable/

bash

## Instructions

    sudo -s
    mkdir -p /root/bin/
    mv regular-patching-script /root/bin
    chmod 700 /root/bin/regular-patching-script
    mkdir -p /etc/regular-patching-script
    mv regular-patching-script.conf /etc/regular-patching-script
    chmod 640 /etc/regular-patching-script/regular-patching-script.conf
    (crontab -l ; echo "0 4 * * * /root/bin/regular-patching-script") | crontab -


## Files
    Perm    File
    0700    /root/bin/regular-patching-script
    0640    /etc/regular-patching-script/regular-patching-script.conf

## License
GPL v2 or successor

## Author
Loye Young
